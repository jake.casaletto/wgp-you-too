use wgpu::{RequestDeviceError, SwapChainError};

impl From<RequestDeviceError> for super::Error {
    fn from(r: RequestDeviceError) -> Self {
        super::Error::RequestDeviceError(r.to_string())
    }
}

impl From<SwapChainError> for super::Error {
    fn from(s: SwapChainError) -> Self {
        match s {
            SwapChainError::Lost => Self::SwapChainLost,
            SwapChainError::OutOfMemory => Self::SwapChainOom,
            SwapChainError::Timeout => Self::SwapChainTimeout,
            SwapChainError::Outdated => Self::SwapChainOutdated,
        }
    }
}
