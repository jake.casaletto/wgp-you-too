use wgpu::{
    util::{BufferInitDescriptor, DeviceExt},
    BackendBit, BlendState, Buffer, BufferUsage, Color, ColorTargetState, ColorWrite,
    CommandEncoderDescriptor, Device, DeviceDescriptor, Features, FragmentState, IndexFormat,
    Instance, Limits, LoadOp, Operations, PipelineLayoutDescriptor, PowerPreference, PresentMode,
    Queue, RenderPassColorAttachment, RenderPassDescriptor, RenderPipeline,
    RenderPipelineDescriptor, RequestAdapterOptions, ShaderFlags, ShaderModuleDescriptor,
    ShaderSource, Surface, SwapChain, SwapChainDescriptor, TextureUsage, VertexState,
};
use winit::{dpi::PhysicalSize, event::WindowEvent, window::Window};

pub mod error;
use crate::resources::vertex::{Vertex, PENTAGON_INDICES, PENTAGON_VERTICES};
use error::{Error, Result};

pub struct PipelineState {
    surface: Surface,
    device: Device,
    queue: Queue,
    swap_chain_descriptor: SwapChainDescriptor,
    swap_chain: SwapChain,
    size: PhysicalSize<u32>,
    render_pipeline: RenderPipeline,
    // TODO wew
    vertex_buffer: Buffer,
    index_buffer: Buffer,
    num_indices: u32,
}
// TODO really bad tutorial code....
// TODO Decoupling: Instance, Surface, Adapter, Device, SwapChain, PresentMode, Color, RenderPipeline, RenderPass, Shader, Buffers
impl PipelineState {
    pub async fn try_new(
        window: &Window,
        required_features: Option<Features>,
        device_limits: Option<Limits>,
    ) -> Result<Self> {
        let size = window.inner_size();
        let instance = Instance::new(BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        let adapter = instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: PowerPreference::default(),
                compatible_surface: Some(&surface),
            })
            .await
            .ok_or(Error::NoSuitableAdapter)?;
        let (device, queue) = adapter
            .request_device(
                &DeviceDescriptor {
                    label: None,
                    features: required_features.unwrap_or(Features::empty()),
                    limits: device_limits.unwrap_or_default(),
                },
                None,
            )
            .await?;
        let swap_chain_descriptor = SwapChainDescriptor {
            usage: TextureUsage::RENDER_ATTACHMENT, // TODO
            format: adapter
                .get_swap_chain_preferred_format(&surface)
                .ok_or(Error::NoSuitableFormat)?,
            width: size.width,
            height: size.height,
            present_mode: PresentMode::Fifo, // TODO
        };
        let swap_chain = device.create_swap_chain(&surface, &swap_chain_descriptor);

        // https://sotrh.github.io/learn-wgpu/beginner/tutorial3-pipeline/#how-do-we-use-the-shaders
        let mut vert_descriptor = wgpu::include_spirv!("../../shaders/triangle/vert.spv");
        vert_descriptor.label = Some("Triangle Vert Shader");

        let vert_shader = device.create_shader_module(&vert_descriptor);

        let mut frag_descriptor = wgpu::include_spirv!("../../shaders/triangle/frag.spv");
        frag_descriptor.label = Some("Triangle Frag Shader");
        let frag_shader = device.create_shader_module(&frag_descriptor);

        let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some("Render pipeline Layout"),
            bind_group_layouts: &[],
            push_constant_ranges: &[],
        });

        let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: Some("Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: VertexState {
                module: &vert_shader,
                entry_point: "main",
                buffers: &[Vertex::desc()],
            },
            primitive: Default::default(),
            depth_stencil: None,
            multisample: Default::default(),
            fragment: Some(FragmentState {
                module: &frag_shader,
                entry_point: "main",
                targets: &[ColorTargetState {
                    format: swap_chain_descriptor.format,
                    blend: Some(BlendState::REPLACE),
                    write_mask: ColorWrite::ALL,
                }],
            }),
        });

        // TODO don't couple buffers here lol
        let vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents: bytemuck::cast_slice(PENTAGON_VERTICES),
            usage: BufferUsage::VERTEX,
        });
        let num_indices = PENTAGON_INDICES.len() as u32; // TODO fix cast...

        let index_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(PENTAGON_INDICES),
            usage: BufferUsage::INDEX,
        });

        Ok(Self {
            surface,
            device,
            queue,
            swap_chain_descriptor,
            swap_chain,
            size,
            render_pipeline,
            vertex_buffer,
            index_buffer,
            num_indices,
        })
    }

    /// https://sotrh.github.io/learn-wgpu/beginner/tutorial2-swapchain/#input
    pub fn resize(&mut self, new_size: (u32, u32)) {
        // Recreate swap chain on resize
        let new_size = PhysicalSize::from(new_size);
        self.size = new_size;
        self.swap_chain_descriptor.width = new_size.width;
        self.swap_chain_descriptor.height = new_size.height;
        self.swap_chain = self
            .device
            .create_swap_chain(&self.surface, &self.swap_chain_descriptor);
    }

    /// https://sotrh.github.io/learn-wgpu/beginner/tutorial2-swapchain/#input
    /// input() returns a bool to indicate whether an event has been fully processed.
    /// If the method returns true, the main loop won't process the event any further.
    pub fn input(&mut self, event: &WindowEvent) -> bool {
        false // TODO
    }

    pub fn update(&mut self) {
        // todo!()
    }

    // TODO - fix from example code
    /// https://sotrh.github.io/learn-wgpu/beginner/tutorial2-swapchain/#render
    pub fn render(&mut self) -> Result<()> {
        let frame = self.swap_chain.get_current_frame()?.output;
        let mut encoder = self
            .device
            .create_command_encoder(&CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        {
            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[RenderPassColorAttachment {
                    view: &frame.view,
                    resolve_target: None,
                    ops: Operations {
                        load: LoadOp::Clear(Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.set_index_buffer(self.index_buffer.slice(..), IndexFormat::Uint16);
            render_pass.draw_indexed(0..self.num_indices, 0, 0..1);
        }

        self.queue.submit(std::iter::once(encoder.finish()));

        Ok(())
    }

    pub fn size(&self) -> (u32, u32) {
        (self.size.width, self.size.height)
    }
}
