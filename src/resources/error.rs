use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error, PartialEq)]
pub enum Error {
    #[error(transparent)]
    WindowError(#[from] crate::resources::window::error::Error),
    #[error(transparent)]
    PipelineStateError(#[from] crate::resources::pipeline_state::error::Error),
}
