mod demo_main_loop;

fn main() -> ! {
    let library_initialization = async {
        wgp_you_too::try_initialize_all::<()>(None, None)
            .await
            .unwrap()
    };
    let initialized_library = futures::executor::block_on(library_initialization);
    demo_main_loop::run_main_loop(initialized_library.window, initialized_library.pipeline);
}
