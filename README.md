"Learn wgpu" following https://sotrh.github.io/learn-wgpu/ 

note: wgsl seems like garbage, prefer to use glsl->spir-v 

https://github.com/gpuweb/gpuweb/issues/566

Dependency: Vulkan SDK (specifically using `glslc`)

https://www.lunarg.com/vulkan-sdk/

Build the shaders with `build.sh` in `shaders/`.  I'll automate it maybe.
